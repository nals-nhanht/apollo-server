// import apollo server module
const { ApolloServer, gql } = require('apollo-server');

const typeDefs = gql`
  type Book {
    title: String
    author: String
  }

  type Query {
    books: [Book]
  }
`;

// defs data set
const books = [
  {
    title: 'Harry Potter and the Goblet of Fire',
    author: 'JK Rowling',
  }, {
    title: 'A Little Life',
    author: 'Hanya Yanagihara',
  }, {
    title: 'Bad Blood',
    author: 'Lorna Sage',
  }, {
    title: 'Chronicles: Volume One',
    author: 'Bob Dylan',
  }, {
    title: 'Noughts & Crosses',
    author: 'Malorie Blackman',
  }, {
    title: 'Priestdaddy',
    author: 'Patricia Lockwood',
  },
];

const resolvers = {
  Query: {
    books: () => books,
  }
}

// provide above infor to apollo server
const server = new ApolloServer({typeDefs, resolvers});

// launche a web server by listen method
server.listen(5000).then(({url}) => {
  console.log(`🚀  Server ready at ${url}`);
});